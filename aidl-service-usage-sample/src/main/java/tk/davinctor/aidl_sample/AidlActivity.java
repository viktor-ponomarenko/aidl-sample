package tk.davinctor.aidl_sample;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

public class AidlActivity extends AppCompatActivity {
    private static final String TAG = "AIDL_ACTIVITY";

    private ISampleAidlService service;

    private ISampleAidlServiceListener.Stub aidlServiceListener = new ISampleAidlServiceListener.Stub() {
        @Override
        public void itemsLoaded(List<Item> items) throws RemoteException {
            try {
                Log.d(TAG, "itemsLoaded");
                String printFormat = "%d: %s";
                int counter = 1;
                for (Item item : items) {
                    Log.d(TAG, String.format(printFormat, counter++, item.toString()));
                    textView.append(item.toString());
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    };

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "onServiceConnected");
            service = ISampleAidlService.Stub.asInterface(iBinder);
            startFetching();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "onServiceConnected");
            service = null;
        }
    };


    private Intent serviceIntent = new Intent()
            .setComponent(new ComponentName("tk.davinctor.aidl_service_implementation",
                  "tk.davinctor.aidl_service_implementation.AidlService"));

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textView);
        textView.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "startService");
        startService(serviceIntent);
        Log.d(TAG, "bindService");
        bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);
    }

    private void startFetching() {
        try {
            service.loadItems(aidlServiceListener);
            //unbindService(serviceConnection);
            //stopService(serviceIntent);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
