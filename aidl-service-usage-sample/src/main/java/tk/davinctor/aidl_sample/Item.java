package tk.davinctor.aidl_sample;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * 28.07.16
 */
public class Item implements Parcelable {

    private String id;
    private String name;
    private List<SubItem> subItems;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SubItem> getSubItems() {
        return subItems;
    }

    public void setSubItems(List<SubItem> subItems) {
        this.subItems = subItems;
    }

    protected Item(Parcel in) {
        id = in.readString();
        name = in.readString();
        subItems = in.createTypedArrayList(SubItem.CREATOR);
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeTypedList(subItems);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("id: ").append(id)
                .append(", name: ").append(name).append("\n")
                .append(" subItems: \n");
        if (subItems == null) {
            builder.append("null");
        } if (subItems.isEmpty()) {
            builder.append("empty");
        } else {
            for (SubItem subItem : subItems) {
                   builder.append("\t")
                           .append(subItem.toString())
                           .append("\n");
            }
        }
        return builder.toString();
    }
}
