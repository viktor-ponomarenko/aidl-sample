package tk.davinctor.aidl_service_implementation;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import tk.davinctor.aidl_sample.ISampleAidlService;
import tk.davinctor.aidl_sample.ISampleAidlServiceListener;
import tk.davinctor.aidl_sample.Item;
import tk.davinctor.aidl_sample.SubItem;

/**
 * 28.07.16
 */
public class AidlService extends Service {
    private static final String TAG = "AIDL_SERVICE";

    private final RandomString randomString = new RandomString(10);
    private final Random rand = new Random();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    private final ISampleAidlService.Stub binder = new ISampleAidlService.Stub() {
        @Override
        public void loadItems(ISampleAidlServiceListener serviceListener) throws RemoteException {
            Log.d(TAG, "loadItems start");
            int count = 20;
            List<Item> items = new ArrayList<>(count);
            for (int i = 0; i < 20; i++) {
                items.add(generateItem());
            }
            serviceListener.itemsLoaded(items);
            Log.d(TAG, "loadItems end");
            stopSelf();
        }
    };

    private Item generateItem() {
        Item item = new Item();
        item.setId(randomString.nextString(rand));
        item.setName(randomString.nextString(rand));
        int count = rand.nextInt(10);
        count = count <= 0 ? 1 : count;
        List<SubItem> subItems = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            subItems.add(generateSubItem());
        }
        item.setSubItems(subItems);
        return item;
    }

    private SubItem generateSubItem() {
        SubItem subItem = new SubItem();
        subItem.setId(randomString.nextString(rand));
        subItem.setName(randomString.nextString(rand));
        return subItem;
    }

}
