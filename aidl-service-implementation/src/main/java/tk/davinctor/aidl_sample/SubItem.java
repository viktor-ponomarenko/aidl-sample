package tk.davinctor.aidl_sample;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 28.07.16
 */
public class SubItem implements Parcelable {

    private String id;
    private String name;

    public SubItem() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private SubItem(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    public static final Creator<SubItem> CREATOR = new Creator<SubItem>() {
        @Override
        public SubItem createFromParcel(Parcel in) {
            return new SubItem(in);
        }

        @Override
        public SubItem[] newArray(int size) {
            return new SubItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
    }

    @Override
    public String toString() {
        return "id: " + id + ", name: " + name;
    }
}
