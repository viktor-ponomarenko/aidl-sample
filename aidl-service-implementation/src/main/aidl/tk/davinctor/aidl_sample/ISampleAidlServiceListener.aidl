// ISampleAidlServiceListener.aidl
package tk.davinctor.aidl_sample;

import java.util.List;
import tk.davinctor.aidl_sample.Item;

interface ISampleAidlServiceListener {
    void itemsLoaded(in List<Item> item);
}
