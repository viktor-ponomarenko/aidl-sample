// ISampleAidlInterface.aidl
package tk.davinctor.aidl_sample;

import tk.davinctor.aidl_sample.ISampleAidlServiceListener;

interface ISampleAidlService {
    void loadItems(in ISampleAidlServiceListener serviceListener);
}
